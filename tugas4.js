function perkenalan(){
    return "Halo, nama saya Zai"
}

var cetakFunction = perkenalan
console.log(cetakFunction())


function tampilkanAngka(angkaPertama, angkaKedua) {
    return angkaPertama + angkaKedua
  }
   
  console.log(tampilkanAngka(20, 7))


  var hello;

hello = () => {
  return "Hello";
}

console.log(hello)


let obj = {
    nama : "john",    
    umur : 22,    
    bahasa : "indonesia"   
    }
console.log(obj.bahasa)


var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , "1992"]
let objDaftarPeserta1 = {
  nama : arrayDaftarPeserta[0]
}
console.log(objDaftarPeserta1);
let objDaftarPeserta2 = {
  jenisKelamin : arrayDaftarPeserta[1]
}
console.log(objDaftarPeserta2);
let objDaftarPeserta3 = {
  hobi : arrayDaftarPeserta[2]
}
console.log(objDaftarPeserta3);
let objDaftarPeserta = {
  tahunLahir : arrayDaftarPeserta[3]
}
console.log(objDaftarPeserta)

var buah = [{nama: "nanas", warna: "kuning", adaBijinya: "tidak", harga: "9000"}, {nama: "jeruk", warna: "oranye", adaBijinya: "ada", harga: "8000"}, {nama: "semangka", warna: "hijau & merah", adaBijinya: "ada", harga: "10000"}, {nama: "pisang", warna: "kuning", adaBijinya: "tidak", harga: "5000"}]
var arrayBuahFilter = buah.filter(function(item){
    return item.adaBijinya != "ada";
 })
 
 console.log(arrayBuahFilter)


 let phone = {
    nama: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020
 };

 const {nama, brand, year} = phone
 console.log(nama, brand, year)


 let dataBukuTambahan= {
    penulis: "john doe",
    tahunTerbit: 2020 
  }
  
  let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172
  }
  
  let objOutput = {...dataBukuTambahan, ...buku}
  console.log(objOutput) 


  let mobil = {

    merk : "bmw",
    
    color: "red",
    
    year : 2002
    
    }
    
     
    
    const functionObject = (param) => {
    
    return param
    
    }

    console.log(functionObject(mobil))