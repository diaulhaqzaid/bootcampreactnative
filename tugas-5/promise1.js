function myCountPromise(num) {
    return new Promise((resolve, reject) => {
      if (typeof num !== 'number') {
        reject('Error: parameter harus berupa angka');
      } else {
        setTimeout(() => {
          resolve(num);
        }, 100);
      }
    });
  }
  myCountPromise(2)
    .then((result) => {
      console.log(result * 2);
    })
    .catch((error) => {
      console.log(error);
    });