var a = 1

while(a < 10){
    console.log(a)
    a = a + 1
}


var a = 1

while(a < 10){
    console.log(a)
    a = a + 2
}


var a = 0

while(a < 10){
    console.log(a)
    a = a + 2
}


let array1 = [1,2,3,4,5,6]
console.log(array1[5])

let array2 = [5,2,4,1,3,5]
array2.sort()
console.log(array2)


let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]
for (let i=0;
    i < array3.length; i++){
    console.log(array3[i])
}


let array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

for(var f = 0; f < array4.length; f+=2) {
    console.log(f);
}


let kalimat= ["saya", "sangat", "senang", "belajar", "javascript"]
var slug = kalimat.join(" ")
console.log(slug)


var sayuran=[]
sayuran.push("kangkung", "Bayam", "Buncis", "Kubis", "Timun", "Seledri", "Tauge")
console.log(sayuran)