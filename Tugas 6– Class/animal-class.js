class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }

    get = () =>{
        return this.name
    }
    get2 = () =>{
        return this.legs
    }
    get3 = () =>{
        return this.cold_blooded
    }
}
 
const sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



class Ape {
    constructor(name, legs) {
        this.name = name
        this.legs = 2
        this.cold_blooded = false
    }
    yell = () => {
        console.log('Auoo')
    }
}
const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"


class Frog {
    constructor(name, legs) {
        this.name = name
        this.legs = 4
        this.cold_blooded = true
    }
    jump = () => {
        console.log('hop hop')
    }
}

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"